INSERT INTO artists (name) VALUES ("Taylor Swift");
INSERT INTO artists (name) VALUES ("Lady Gaga");
INSERT INTO artists (name) VALUES ("Justin Bieber");
INSERT INTO artists (name) VALUES ("Ariana Grande");
INSERT INTO artists (name) VALUES ("Bruno Mars");

-- Taylor Swift

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Fearless", "2008", 3);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Fearless", 246, "Pop rock", 3);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Love Story", 213, "Country pop", 3);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Red", "2012", 3);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("State of Grace", 273, "Rock, alternative rock, arena rock", 4);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Red", 204, "Country", 4);

-- Lady Gaga

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("A Star Is Born", "2018", 4);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Black Eyes", 181, "Rock and roll", 5);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Shallow", 201, "Country, rock, folk rock", 5);	

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Born This Way", "2011", 4);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Born This Way", 252, "Electropop", 6);

-- Justin Bieber

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Purpose", "2015", 5);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Sorry", 192, "Dancehall-poptropical housemoombahton", 7);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Believe", "2012", 5);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Boyfriend", 251, "Pop", 8);

-- Ariana Grande

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Dangerous Woman", "2016", 6);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Into You", 242, "EDM house", 9);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Thank U, Next", "2019", 6);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Thank U Next", 196, "Pop, R&B", 10);

-- Bruno Mars

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("24K Magic", "2016", 7);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("24K Magic", 207, "Funk, disco, R&B", 11);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Earth to Mars", "2011", 7);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Lost", 192, "Pop", 12);

------- Advanced Selects
-- use the ! symbol to exclude records from selection
SELECT * FROM songs WHERE id != 11;

-- OR Operator
SELECT * FROM songs WHERE id = 1 OR id = 3 OR id = 5;

-- IN Operator
SELECT * FROM songs WHERE id IN (1, 2, 3);
SELECT * FROM songs WHERE genre IN ("Pop", "K-Pop");

-- Combining conditions
SELECT * FROM songs WHERE album_id = 4 AND id > 6;

-- Find partial matches
-- get songs with s at the end of their song_name
SELECT * FROM songs WHERE song_name LIKE "%s";

-- get songs with k at the start of their song_name
SELECT * FROM songs WHERE song_name LIKE "k%";

-- get songs where there is/are letter a in the middle of their song_name
SELECT * FROM songs WHERE song_name LIKE "%a%";


-- Sorting records
SELECT * FROM songs ORDER BY song_name ASC;
SELECT * FROM songs ORDER BY id DESC;


-- Table Joins
SELECT * FROM artists
    JOIN albums ON artists.id = albums.artist_id;

SELECT * FROM artists
    JOIN albums ON artists.id = albums.artist_id
    JOIN songs ON albums.id = songs.album_id;


-- Since we are joining the tables, it means that we can access any property/column that exists in either of the tables in aone single SQL query
SELECT artists.name, albums.album_title FROM artists
    JOIN albums ON artists.id = albums.artist_id;